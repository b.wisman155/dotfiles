read -p "Are you sure? " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cp init.vim ~/.config/nvim/ 
    cp alacritty.yml ~/.config/alacritty/
    cp omf.fish ~/.config/fish/conf.d/
    cp tmux.conf ~/ 
fi
