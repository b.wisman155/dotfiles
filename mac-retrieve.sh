read -p "Are you sure? " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cp ~/.config/nvim/init.vim .
    cp ~/.config/alacritty/alacritty.yml .
    cp ~/.config/fish/conf.d/omf.fish .
    cp ~/.tmux.conf .
fi
