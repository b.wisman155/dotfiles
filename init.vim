"No Compatibility, very annoying on RH/Windows/MacOS
set nocompatible

" All Plugins!
call plug#begin()
Plug 'scrooloose/nerdtree'
Plug 'mikelue/vim-maven-plugin'
Plug 'justinmk/vim-sneak'

" Syntax files
Plug 'sheerun/vim-polyglot'

" Autocomplete
Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}

" Misc
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhartington/oceanic-next'
Plug 'eugen0329/vim-esearch'
" Plug 'lifepillar/vim-solarized8'
Plug 'srcery-colors/srcery-vim'

" Skim Plug 'lotabout/skim', { 'dir': '~/.skim', 'do': './install' }
Plug 'lotabout/skim.vim'

" LaTeX
Plug 'lervag/vimtex'

" Programming tools
Plug 'rust-lang-nursery/rustfmt'
Plug 'Yggdroot/indentLine'
Plug 'mattn/emmet-vim'
Plug 'terryma/vim-multiple-cursors'


call plug#end()

" Required for operations modifying multiple buffers like rename.
set hidden
set conceallevel=2

" Vim config
set nu
set relativenumber " Relative line numbering
set number " Now we have hybrid numbering!
set tabstop=4 
set softtabstop=0 
set expandtab 
set shiftwidth=4 
set smarttab
set autoindent
set showmatch
set scrolloff=5 " Always have 5 lines above and below the cursor
set encoding=utf8
"set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Plus\ Nerd\ File\ Types:h11
set clipboard=unnamed
nmap <silent> <A-Up> : wincmd <C-W>k<CR>
nmap <silent> <A-Down> :wincmd <C-W>j<CR>
nmap <silent> <A-Left> :wincmd <C-W>h<CR>
nmap <silent> <A-Right> :wincmd <C-W>l<CR>
set visualbell " Visual bell instead of beeping
set noerrorbells " Don't ring the error bells

set ttyfast " Make stuff faster
set autoread " Automatically reread file if changed outisde vim
set nobackup
set noswapfile

let mapleader="," " Change leader to ,
nnoremap <silent><Tab> :bn<CR>
nnoremap <silent><S-Tab> :bp<CR>
nnoremap <silent>d<Tab> :bd<CR>
nnoremap <silent>!d<Tab> :bd!<CR>
nnoremap <silent><leader>f :SK<CR>
nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap <up> <nop>
nnoremap <down> <nop>
imap § <Esc>
" autocmd BufNewFile,BufRead *.vue set filetype=html

" syntax config
let python_highlight_all = 1

"Deoplete config
"let g:deoplete#enable_at_startup=1
"let g:deoplete#enable_ignore_case=1
"let g:deoplete#enable_camel_case=1

" Echo doc config
set noshowmode

" Terminal esc
tnoremap <Esc> <C-\><C-n>

" Shortcuts
nmap <C-m> :NERDTreeToggle<CR>
nn <silent><leader>l :setl rnu!<cr>
nn <silent><leader>n :nohl<cr>

" Git shortcuts
map <leader>gs :!git status<cr>
map <leader>ga :!git add %<cr>
map <expr> <leader>gc GitCommit(input("Commit message: "))
map <leader>gp :!git push

function! GitCommit(message) 
	execute "!git commit -m \"" . a:message . "\""
endfunction

" Programming tools config
let g:rustfmt_autosave = 1

" LSP config
" let g:LanguageClient_serverCommands = {
"    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
"    \ 'python': ['pyls'],
"    \ 'c': ['ccls', '--log-file=/tmp/cc.log'],
"    \ 'cpp': ['ccls', '--log-file=/tmp/cc.log'],
"    \ }


" let g:LanguageClient_autoStart = 1
" let g:LanguageClient_diagnosticsEnable = 0

" nnoremap <leader>ld :call LanguageClient#textDocument_definition()<CR>
" nnoremap <leader>lr :call LanguageClient#textDocument_rename()<CR>
" nnoremap <leader>lf :call LanguageClient#textDocument_formatting()<CR>
" nnoremap <leader>lt :call LanguageClient#textDocument_typeDefinition()<CR>
" nnoremap <leader>lx :call LanguageClient#textDocument_references()<CR>
" nnoremap <leader>la :call LanguageClient_workspace_applyEdit()<CR>
" nnoremap <leader>lc :call LanguageClient#textDocument_completion()<CR>
" nnoremap <leader>lh :call LanguageClient#textDocument_hover()<CR>
" nnoremap <leader>ls :call LanguageClient_textDocument_documentSymbol()<CR>
" nnoremap <leader>lm :call LanguageClient_contextMenu()<CR>

" Theme
if has("termguicolors")
    set termguicolors
endif

set t_Co=256
syntax on
colorscheme srcery
set background=dark

let g:srcery_inverse = 0

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme="bubblegum"
let g:airline#extensions#tabline#ignore_bufadd_pat = 'defx|gundo|nerd_tree|startify|tagbar|undotree|vimfiler'

" NERDTree Highlight
let g:NERDTreeHighlightFolders = 1 " enables folder icon highlighting using exact match
let g:NERDTreeHighlightFoldersFullName = 1 " highlights the folder name
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1

" FZF
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
command! -bang -nargs=* Ag call fzf#vim#ag_interactive(<q-args>, fzf#vim#with_preview('right:50%:hidden', 'alt-h'))
command! -bang -nargs=* Rg call fzf#vim#rg_interactive(<q-args>, fzf#vim#with_preview('right:50%:hidden', 'alt-h'))

" LaTeX/Skim
let g:vimtex_enabled = 1
let g:vimtex_fold_enabled = 1
let g:vimtex_fold_manual = 1
let g:vimtex_text_obj_enabled = 0
let g:vimtex_imaps_enabled = 0
let g:vimtex_motion_enabled = 1
let g:vimtex_mappings_enabled = 0
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_method = 'skim'
let g:polyglot_disabled = ['latex']


" IndentLine
let g:indentLine_setColors = 1
let g:indentLine_color_term = 239
let g:indentLine_char = '¦'
let g:indentLine_concealcursor = "nv"
let g:indentLine_conceallevel = 2

""""""""""""""""""""""""""""""""""""""
" COC CONFIG SECTION
""""""""""""""""""""""""""""""""""""""

inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"


" if hidden is not set, TextEdit might fail.
set hidden

" Better display for messages
set cmdheight=2

" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# 's'
endfunction

" Use <c-space> for trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "<C-y>" : "<C-g>u<CR>"

" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
vmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use `:Format` for format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` for fold current buffer
command! -nargs=? Fold :call CocAction('fold', <f-args>)


" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
