# Path to Oh My Fish install.
set -q XDG_DATA_HOME
  and set -gx OMF_PATH "$XDG_DATA_HOME/omf"
  or set -gx OMF_PATH "$HOME/.local/share/omf"

# Load Oh My Fish configuration.
source $OMF_PATH/init.fish
source /Users/bertuswisman/.cargo/env
export PATH="/usr/local/opt/qt/bin:$PATH"
export RUST_SRC_PATH="/Users/bertuswisman/.rustup/toolchains/nightly-x86_64-apple-darwin/lib/rustlib/src/rust/src"

abbr --add projects "cd /Volumes/MBA\ Bertus\ Wisman/projects/projects/" 
abbr --add venv ". venv/bin/activate.fish"
